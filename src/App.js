import React from 'react';
import './App.css';
import StudentList from './StudentList';
// import StudentDetail from './StudentDetail';

function App() {
  return (
    <div className="App">
      <StudentList />
      {/* <StudentDetail /> */}
    </div>
  );
}

export default App;
